import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Actions from "./Actions";
import Plan from "./Plan";
import {
  List,
  ListItem
} from 'material-ui/List';
import ContentAdd from 'material-ui/svg-icons/content/add';
import FloatingActionButton from 'material-ui/FloatingActionButton';

class PlanList extends Component {
  componentWillMount() {
    console.log("componentWillMount")
    this.getStoreData();
  }
  getStoreData() {
    this.props.dispatch(Actions.getAllPlans());
  }
  deletePlanHandler = (planId) => {
    this.props.dispatch(Actions.deletePlan(planId));
  }
  editPlanHandler = (plan) => {
    this.props.dispatch(Actions.editPlan(plan));
  }
  render() {
    const { planList } = this.props;
    const planItems = planList.length>0?planList.map((plan, index) => {
      return <Plan
                key={index}
                plan={plan}
                onDeleleClick={this.deletePlanHandler}
                onEditClick={this.editPlanHandler} />
    }):<ListItem primaryText={"No plans found!!"} />;
    return (
      <div>
        <List>
          {planItems}
        </List>

        <FloatingActionButton mini={true} onTouchTap={this.props.handleAddPlanDialog}>
          <ContentAdd />
        </FloatingActionButton>
      </div>
    );
  }
}

function mapStateToProps(state, prop) {
  return {...state};
}
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(Actions),
    dispatch
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PlanList);
