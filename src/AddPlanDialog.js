import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Actions from "./Actions";
import Divider from 'material-ui/Divider';
import Dialog from 'material-ui/Dialog';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import ContentAdd from 'material-ui/svg-icons/content/add';


const style = {
  textField: {
    width: "100%"
  },
  button: {
    margin: "10px"
  }
};
class AddPlan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      desc: ""
    }
  }
  componentWillMount() {
    console.log("componentWillMount");
  }
  componentWillReceiveProps(newProps) {
    const {planList} = this.props;
    if(planList.length !== newProps.planList.length) {
      this.setState({
        title: "",
        desc: ""
      });
    }
  }
  handleChange = (type, e, value) => {
    if(type === "title") {
      this.setState({
        title: value
      });
    } else {
      this.setState({
        desc: value
      });
    }
  }
  savePlanHandler = (e) => {
    this.props.dispatch(Actions.addPlan({...this.state}));
    this.props.dispatch(Actions.closeAddPlanDialog(false));
  }
  render() {
    const actionBtns = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.props.handleAddPlanDialog.bind(this, false)} />,
      <FlatButton
        label="Submit"
        primary={true}
        onTouchTap={this.savePlanHandler} />,
    ];
    return (
      <Dialog
        title="Add Your Plan"
        actions={actionBtns}
        modal={true}
        open={this.props.addPlanDialogOpen}
      >
        <div className="start-xs">
          <div className="col-xs-12">
            <TextField
              style={style.textField}
              hintText="Title"
              underlineShow={false}
              value={this.state.title}
              onChange={this.handleChange.bind(this, "title")}
              onKeyUp={this.handleKeyUp} />
          </div>
        </div>
        <Divider />
        <div className="start-xs">
          <div className="col-xs-12">
            <TextField
              style={style.textField}
              hintText="Description"
              underlineShow={false}
              value={this.state.desc}
              onChange={this.handleChange.bind(this, "desc")}
              onKeyUp={this.handleKeyUp} />
          </div>
        </div>
        <Divider />
      </Dialog>
    );
  }
}

function mapStateToProps(state, prop) {
  return {
    ...state
  }
}

function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(Actions),
    dispatch
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddPlan);