export default {
  addPlan: (plan) => {
    return {
      type: "ADD_PLAN",
      plan
    }
  },
  editPlan: (plan) => {
    return {
      type: "EDIT_PLAN",
      plan
    }
  },
  deletePlan: (planId) => {
    return {
      type: "DELETE_PLAN",
      planId
    }
  },
  getAllPlans: () => {
    return {
      type: "GET_ALL_PLANS"
    }
  },
  openAddPlanDialog: (isOpen) => {
    return {
      type: "OPEN_ADD_PLAN_DIALOG",
      isOpen
    }
  },
  closeAddPlanDialog: (isOpen) => {
    return {
      type: "CLOSE_ADD_PLAN_DIALOG",
      isOpen
    }
  }
};
