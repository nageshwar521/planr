import React, { Component } from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import AddPlanDialog from "./AddPlanDialog";
import PlanList from "./PlanList";
import Actions from "./Actions";
import AppBar from 'material-ui/AppBar';
import './App.css';

const style = {
  app: {
  }
};

class App extends Component {
  constructor() {
    super();
  }
  addPlanDialogHanler = (isOpen = false) => {
    this.props.dispatch(Actions.closeAddPlanDialog(isOpen));
  }
  render() {
    const {addPlanDialogOpen} = this.props;
    return (
      <div className="planr" style={style.app}>
        <AppBar
          title="Plan" />
        <AddPlanDialog 
          addPlanDialogOpen={addPlanDialogOpen} 
          handleAddPlanDialog={this.addPlanDialogHanler} />
        <PlanList handleAddPlanDialog={this.addPlanDialogHanler} />
      </div>
    );
  }
}

function mapStateToProps(state, prop) {
  return {
    ...state
  }
}

function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(Actions),
    dispatch
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
