import { combineReducers } from 'redux';

let initialState = {
  planList: [],
  addPlanDialogOpen: false
};

function updatePlanList(planList, newPlan) {
  return planList.map( (plan, index) => {
    if(plan.id === newPlan.id) plan.title = newPlan.title;
    return plan;
  } );
}

function deletePlan(planList, planId) {
  return planList.filter( plan => plan.id !== planId );
}

const planReducer = (state = initialState, action) => {
  console.log("reducer called!!");
  switch (action.type) {
    case "ADD_PLAN": {
      //add plan
      const { planList } = state;
      const count = planList.length;
      const { plan } = action;
      plan.id = count + 1;
      return { planList: [plan, ...planList] };
    }
    case "EDIT_PLAN": {
      //edit plan
      const { planList } = state;
      const newPlanList = updatePlanList(planList, action.plan);
      return { planList: [...newPlanList] };
    }
    case "DELETE_PLAN": {
      //delete plan
      const { planList } = state;
      const { planId } = action;
      const newPlanList = deletePlan(planList, planId);
      return { planList: [...newPlanList] };
    }
    case "GET_ALL_PLANS": {
      //returns all plans in store
      return state;
    }
    case "OPEN_ADD_PLAN_DIALOG": {
      //open dialog
      const { isOpen } = action;
      return Object.assign({}, state, {addPlanDialogOpen: isOpen});
    }
    case "CLOSE_ADD_PLAN_DIALOG": {
      //close dialog
      const { isOpen } = action;
      return Object.assign({}, state, {addPlanDialogOpen: isOpen});
    }
    default:

  }
  console.log("state");
  console.log(state);
  return state;
};


// const rootReducer = combineReducers({
//   planReducer
// });

export default planReducer;
