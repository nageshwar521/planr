import React, { Component } from "react";
import FaClose from "react-icons/lib/fa/close";
import FaEdit from "react-icons/lib/fa/edit";
import {
  List,
  ListItem
} from 'material-ui/List';
import Checkbox from 'material-ui/Checkbox';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import IconButton from 'material-ui/IconButton';
import ContentClear from 'material-ui/svg-icons/content/clear';
import ContentCreate from 'material-ui/svg-icons/content/create';

const style = {
  listItem: {
    color: "#FFF",
    borderBottom: "1px solid #107280"
  }
};

export default class Plan extends Component {
  constructor() {
    super();
    this.state = {
      checked: false
    }
  }
  onCheckChange = () => {
    const { plan } = this.props;
    this.setState({
      checked: !this.state.checked
    });
  }
  render() {
    const { plan } = this.props;
    const { checked } = this.state;
    return <ListItem
              style={style.listItem}
              primaryText={plan.title} />
  }
}